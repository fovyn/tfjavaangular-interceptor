import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/shared/modules/securite/components/login/login.component';
import { RoleGuard } from 'src/app/shared/modules/securite/guards/role.guard';
import { UserGuard } from 'src/app/shared/modules/securite/guards/user.guard';

const routes: Routes = [
  { path: 'home', canActivate: [UserGuard], loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)},
  { path: 'secured', canActivate: [UserGuard, RoleGuard], data: {roles: ["ROLE_STANDARD"]}, loadChildren: () => import('./pages/secured/secured.module').then(m => m.SecuredModule) },
  { path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
