import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tfjavaangular2021';
  private eleves: string[] = [];
  get Eleves(): string[] { return this.eleves;}

  AddEleve(nom: string) {
    this.eleves.push(nom);
  }
}
