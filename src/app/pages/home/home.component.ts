import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterState } from '@angular/router';
import { HomeService } from 'src/app/pages/home/services/home.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [HomeService]
})
export class HomeComponent implements OnInit {

  constructor(private $home: HomeService) { }

  ngOnInit(): void {
    
  }

}
