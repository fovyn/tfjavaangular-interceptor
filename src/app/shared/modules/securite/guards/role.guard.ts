import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import jwtDecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/modules/securite/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  private token: string | null = null;
  constructor(private $auth: AuthService, private $router: Router) {
    $auth.Token$.subscribe(token => this.token = token);
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const decoded: {roles: string[]} = jwtDecode(this.token as string);
      const uRoles = decoded.roles;
      const rRoles = route.data['roles'];

      for(let rRole of rRoles) {
        for(let uRole of uRoles) {
          if (uRole === rRole) {
            return true;
          }
        }
      }
      
    return this.$router.navigate(['/login']);
  }
  
}
