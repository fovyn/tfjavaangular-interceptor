import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { INJECT_SECURITE_ApiUrl } from 'src/app/shared/modules/securite/securite.module';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token$ = new BehaviorSubject<string | null>(null);
  get Token$(): Observable<string | null> { return this.token$.asObservable(); }
  set Token(v: string | null) { this.token$.next(v); }

  constructor(
    @Inject(INJECT_SECURITE_ApiUrl) private url: string,
    private $http: HttpClient
    ) { }

  async login(username: string, pwd: string): Promise<any> {
    return this.$http
      .get<any>(`${this.url}/users?username=${username}&password=${pwd}`)
      .pipe(
        map(a => a[0]),
        tap(res => this.Token = res.token)
      )
      .toPromise();
  }

  logout() {
    this.Token = null;
  }

}

