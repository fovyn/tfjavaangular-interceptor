import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { F_Login } from 'src/app/shared/modules/securite/forms/user.form';
import { AuthService } from 'src/app/shared/modules/securite/services/auth.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  fLogin = new FormGroup(F_Login);

  constructor(private $auth: AuthService, private $router: Router) {
    this.$auth.Token$.subscribe(token => console.log(token));
  }

  get Username(): FormControl {
    return this.fLogin.get('username') as FormControl;
  }

  ngOnInit(): void {
  }


  async submitAction() {
    try {
      if(this.fLogin.invalid) return;

      const res = await this.$auth.login("flavian", "ovyn");
  
      console.log(res);
      this._successLoginAction(res);
    } catch(e) {
      this._errorLoginAction(e);
    }
  }

  _successLoginAction(res: any) {
    this.$router.navigate(['/home'], {state: {"roles": "blop"}}).then(_ => console.log('Redirect to /home'));
  }
  _errorLoginAction(res: any) {

  }
}
