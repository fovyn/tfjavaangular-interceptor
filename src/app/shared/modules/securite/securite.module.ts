import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecuriteRoutingModule } from './securite-routing.module';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from 'src/app/shared/modules/securite/interceptors/jwt.interceptor';
import { CustomFormsModule } from 'src/app/shared/modules/custom-forms/custom-forms.module';

export interface SecuriteOptions {
  apiUrl: string
}
export const INJECT_SECURITE_ApiUrl = "securite.apiUrl";

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    SecuriteRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CustomFormsModule
  ]
})
export class SecuriteModule {
  static forRoot(opts: SecuriteOptions): ModuleWithProviders<SecuriteModule> {
    return {
      ngModule: SecuriteModule,
      providers: [
        { provide: INJECT_SECURITE_ApiUrl, useValue: opts.apiUrl },
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      ]
    }
  }
}
