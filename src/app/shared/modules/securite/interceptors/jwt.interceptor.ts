import { Injectable, OnDestroy } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/modules/securite/services/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor, OnDestroy {
  private token: string | null = null;
  private token$$: Subscription;

  constructor(private $auth: AuthService) {
    this.token$$ = $auth.Token$.subscribe(token => this.token = token);
  }
  ngOnDestroy(): void {
    this.token$$.unsubscribe();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.token) {
      request = request.clone({setHeaders: {Authorization: `${this.token}`}});
    }

    return next.handle(request);
  }
}
