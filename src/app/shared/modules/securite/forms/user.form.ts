import { FormControl, FormGroup, Validators } from "@angular/forms";
import { FormDeclaration } from "src/app/shared/modules/custom-forms/types/form.type";

export const F_Login: FormDeclaration = {
    username: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required, Validators.minLength(4)])
};

export const F_UserAddress: FormDeclaration = {
    rue: new FormControl(null),
    numero: new FormControl(null)
};

export const F_Register: FormDeclaration = {
    userInfo: new FormGroup(F_Login),
    addresse: new FormGroup(F_UserAddress)
};