import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormInputComponent } from 'src/app/shared/modules/custom-forms/components/form-input/form-input.component';



@NgModule({
  declarations: [FormInputComponent],
  imports: [
    CommonModule
  ],
  exports: [FormInputComponent]
})
export class CustomFormsModule { }
