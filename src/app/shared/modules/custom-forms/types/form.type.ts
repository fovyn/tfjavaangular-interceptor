import { AbstractControl } from "@angular/forms";

export type FormDeclaration = {[key: string]: AbstractControl};

export type FormInputType = 'text' | 'number' | 'password';