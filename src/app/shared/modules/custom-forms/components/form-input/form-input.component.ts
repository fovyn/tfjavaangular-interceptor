import { Component, Input, OnInit, Optional } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { FormInputType } from 'src/app/shared/modules/custom-forms/types/form.type';
import {v4} from 'uuid';

@Component({
  selector: 'form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements OnInit, ControlValueAccessor {
  private id = v4();
  get Id(): string { return this.id; }

  private label: string | null = null;
  private type: FormInputType = 'text';
  private name: string | null = null;
  
  value: any;
  onChange: any;
  onTouched: any;

  @Input()
  set Type(v: FormInputType) { this.type = v; }
  get Type(): FormInputType { return this.type; }

  @Input()
  set Label(v: string) { this.label = v; }
  get Label(): string { return this.label as string; }
  @Input()
  set Name(v: string) { this.name = v; }
  get Name(): string { return this.name as string; }

  constructor(@Optional() private ngControl: NgControl) {
    this.ngControl.valueAccessor = this;
  }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnInit(): void {
  }

}
