import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecuriteModule } from 'src/app/shared/modules/securite/securite.module';
import { CustomFormsModule } from 'src/app/shared/modules/custom-forms/custom-forms.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SecuriteModule.forRoot({apiUrl: "http://localhost:3000"}),
    CustomFormsModule
  ],
  exports: [
    SecuriteModule,
    CustomFormsModule
  ]
})
export class SharedModule { }
